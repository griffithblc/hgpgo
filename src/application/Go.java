package application;
	
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;


public class Go extends Application {
	// private fields for a stack pane and a reversi control
	private StackPane sp_mainlayout;
	private CustomControl ccGo;
	
	//Essai du gitan
	HBox PlainBox;
	VBox VertBox;
	//interface
	Button bt1; //Pass
	Button bt2; //Reset
	Button bt3; //Show Rules
	Label lbl;
	Label scorelbl; //Label score
	Label turn; //Label status
	Label colorTurn; //Label turn
	Label j1score; //j1score
	Label j2score; //j2score
	Label RulesTitle;
	Label Rules;
	boolean showRules;
	//-Images
	ImageView rulesimg;	//Image rulesimg;
	ImageView winblackimg;//Image winblack
	ImageView winwhiteimg;//Image winwhite
	ImageView equalityimg;//Image equality
		
	// overridden init method
	public void init() {
		
		PlainBox = new HBox(8);
		VertBox = new VBox(8);
		
		//menu
		bt1 = new Button("Pass");
		bt2 = new Button("Reset");
		bt3 = new Button("Rules");
		lbl = new Label("Menu");
		lbl.setFont(new Font("Arial", 30));
		scorelbl = new Label("Score");
		scorelbl.setFont(new Font("Arial", 20));
		j1score = new Label();
		j2score = new Label();
		turn = new Label("Turn");
		turn.setFont(new Font("Arial", 20));
		colorTurn = new Label();
		VertBox.setAlignment(Pos.TOP_CENTER);
		VertBox.setPrefWidth(100);
		
		//game
		ccGo = new CustomControl();
		ccGo.getBoard().setGo(this);
		HBox.setHgrow(ccGo, Priority.ALWAYS);
		
		//images
		rulesimg = new ImageView(new Image("rules.png"));
		winblackimg = new ImageView(new Image("blackwin.png"));
		winwhiteimg = new ImageView(new Image("whitewin.png"));
		equalityimg = new ImageView(new Image("equality.png"));
		
		//add childrens to layouts
		PlainBox.getChildren().add(ccGo);
		PlainBox.getChildren().add(VertBox);
		VertBox.getChildren().add(lbl);
		VertBox.getChildren().add(scorelbl);
		VertBox.getChildren().add(j1score);
		VertBox.getChildren().add(j2score);
		VertBox.getChildren().add(turn);
		VertBox.getChildren().add(colorTurn);
		VertBox.getChildren().add(bt1);
		VertBox.getChildren().add(bt3);
		VertBox.getChildren().add(bt2);
		sp_mainlayout = new StackPane();
		rulesimg.setOpacity(0);
		sp_mainlayout.getChildren().add(rulesimg);
		sp_mainlayout.getChildren().add(winblackimg);
		sp_mainlayout.getChildren().add(winwhiteimg);
		sp_mainlayout.getChildren().add(equalityimg);
		sp_mainlayout.getChildren().add(PlainBox);
		showRules = false;
		
		//-Event:
		//Pass
		bt1.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event)
			{
				ccGo.getBoard().getGameLogic().pass(); //pass turn
				refresh();
			}
		});
		//Reset
		bt2.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event)
			{
				ccGo.getBoard().resetGame();
				bt1.setDisable(false);
				bt3.setDisable(false);
				refresh();
				hideImages();
			}
		});
		//Rules
		bt3.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event)
			{
				if (showRules)
				{
					showRules = false;
					rulesimg.toBack();
					rulesimg.setOpacity(0);
					bt1.setDisable(false);
					bt2.setDisable(false);
				}
				else
				{
					showRules = true;
					rulesimg.toFront();
					rulesimg.setOpacity(100);
					bt1.setDisable(true);
					bt2.setDisable(true);
				}
			}
		});
		
		//hide Images
		hideImages();
		
		//First refresh
		refresh();
	}
	
	public void hideImages()
	{
		winblackimg.toBack();
		winblackimg.setOpacity(0);
		winwhiteimg.toBack();
		winwhiteimg.setOpacity(0);
		equalityimg.toBack();
		equalityimg.setOpacity(0);
	}
	
	public void refresh()
	{
		j1score.setText("Black: " + this.ccGo.getBoard().getGameLogic().getblackscore());
		j2score.setText("White: " + this.ccGo.getBoard().getGameLogic().getwhitescore());
		colorTurn.setText(this.ccGo.getBoard().getGameLogic().getcurrplayer().toString());
		if (this.ccGo.getBoard().getGameLogic().getwinstatus() == WinStatus.BLACK_WIN)
		{
			//Show BLACK WIN image
			winblackimg.toFront();
			winblackimg.setOpacity(100);
			bt1.setDisable(true);
			bt3.setDisable(true);
		}
		else if (this.ccGo.getBoard().getGameLogic().getwinstatus() == WinStatus.WHITE_WIN)
		{
			//Show White Win image
			winwhiteimg.toFront();
			winwhiteimg.setOpacity(100);
			bt1.setDisable(true);
			bt3.setDisable(true);
		}
		else if ((this.ccGo.getBoard().getGameLogic().getwinstatus() == WinStatus.EQUAL_SCORE))
		{
			//Show EqualScore image
			equalityimg.toFront();
			equalityimg.setOpacity(100);
			bt1.setDisable(true);
			bt3.setDisable(true);
		}
	}
	
	@Override
	public void start(Stage primaryStage) {
		Scene scene = new Scene(sp_mainlayout, 800, 800);
		primaryStage.setTitle("Go");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}

