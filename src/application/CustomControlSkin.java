package application;

import javafx.scene.control.SkinBase;

public class CustomControlSkin  extends SkinBase<CustomControl>
{
		// default constructor for the class
		public CustomControlSkin(CustomControl cc) {
			super(cc);
		}
}