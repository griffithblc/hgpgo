package application;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.transform.Translate;

public class Stone extends Group 
{
	private PieceType player;
	private Ellipse piece;
	private Translate t;
	private boolean checked;
	
	public Stone()
	{
		this.piece = new Ellipse();
		this.t = new Translate();
		this.piece.getTransforms().add(this.t);
        getChildren().add(this.piece);
        this.setPiece(PieceType.EMPTY);
        checked = false;
	}
	
	// overridden version of the resize method to give the piece the correct size
	@Override
	public void resize(double width, double height) {
		super.resize(width, height);
		this.piece.setCenterX(width / (double)2);
		this.piece.setRadiusX(width / (double)2);
		this.piece.setCenterY(height / (double)2);
		this.piece.setRadiusY(height / (double)2);
	}
	
	// overridden version of the relocate method to position the piece correctly
	@Override
	public void relocate(double x, double y) {
		super.relocate(x, y);
		this.t.setX(x);
		this.t.setY(y);
	}
	
	// public method that will swap the colour and type of this piece
	public void swapPiece() {
		if (this.player == PieceType.BLACK)
			this.setPiece(PieceType.WHITE);
		else if (this.player == PieceType.WHITE)
			this.setPiece(PieceType.BLACK);
	}
	
	// method that will set the piece type
	public void setPiece(final PieceType type) {
		this.player = type;
		if (this.player == PieceType.WHITE)
        	this.piece.setFill(Color.WHITE);
        else if (this.player == PieceType.BLACK)
        	this.piece.setFill(Color.BLACK);
        else
        	this.piece.setFill(Color.TRANSPARENT);
	}
	
	//check value of piece
	public boolean is(PieceType pt)
	{
		return this.player == pt;
	}
	
	public boolean isNot(PieceType pt)
	{
		return this.player != pt;
	}
	
	public void reset()
	{
		this.setPiece(PieceType.EMPTY);
	}
	
	// returns the type of this piece
	public PieceType getPlayer() {
		// NOTE: this is to keep the compiler happy until you get to this point
		return this.player;
	}

	public boolean getChecked() {
		return checked;
	}
	
	public void setChecked(boolean check) {
		checked = check;
	}
}
