package application;

import javafx.event.EventHandler;
import javafx.scene.control.Control;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public class CustomControl extends Control {
	// private fields of a GoBoard
	GoBoard goBoard;
	
	// constructor for the class
	public CustomControl() {
		setSkin(new CustomControlSkin(this));
		goBoard = new GoBoard();
		getChildren().add(goBoard);
		// add a mouse clicked listener that will try to place a piece
		setOnMouseClicked(new EventHandler<MouseEvent>() {
		// overridden handle method
		@Override
		public void handle(MouseEvent event) {
			goBoard.placePiece(event.getX(), event.getY());
		}
		});
		setOnKeyPressed(new EventHandler<KeyEvent>() {
		// overridden handle method
		@Override
		public void handle(KeyEvent event) {
		//if(event.getCode() == KeyCode.SPACE)
			goBoard.resetGame();
		}
		});
	}
	
	// overridden version of the resize method
	@Override
	public void resize(double width, double height) {
		super.resize(width, height);
		goBoard.resize(width, height);
	}
	
	public GoBoard getBoard()
	{
		return goBoard;
	}
}
