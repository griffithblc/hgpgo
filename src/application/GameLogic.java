package application;


import java.util.ArrayList;
import java.util.List;

import com.sun.media.jfxmedia.events.PlayerStateEvent.PlayerState;

public class GameLogic
{
	//Variables
	private int[] playerScore;
	private Stone[][] stones;
	private int tmpx, tmpy;
	private List<int[]> alreadyChecked;
	private List<int[]> toRemove;
	private int[] tmp_score = {0,0};
	Stone[][] tmp_board1;
	Stone[][] tmp_board2;
	private int passed;
	private WinStatus winstatus;
	private PieceType currPlayer;

	//Getters
	public int getblackscore()
	{
		int black_score = playerScore[0];
		
		for (int i = 0; i < stones.length; i++)
			for (int j = 0; j < stones[i].length; j++)
				if (stones[i][j].getPlayer() == PieceType.BLACK)
					black_score++;
		return black_score;
	}
	
	public int getwhitescore()
	{
		int white_score = playerScore[1];
		
		for (int i = 0; i < stones.length; i++)
			for (int j = 0; j < stones[i].length; j++)
				if (stones[i][j].getPlayer() == PieceType.WHITE)
					white_score++;
		return white_score;
	}
	
	public WinStatus getwinstatus()
	{
		return winstatus;
	}
	
	public PieceType getcurrplayer()
	{
		return currPlayer;
	}
	
	private void nextPlayer()
	{
		this.currPlayer = this.currPlayer == PieceType.BLACK ? PieceType.WHITE : PieceType.BLACK;
	}
	
	private PieceType getEnnemyColor()
	{
		return this.currPlayer == PieceType.BLACK ? PieceType.WHITE : PieceType.BLACK;
	}
	
	private void addCurrentPlayerScore(final int scoreToAdd)
	{
		if (this.currPlayer == PieceType.BLACK)
			this.playerScore[0] += scoreToAdd;
		else
			this.playerScore[1] += scoreToAdd;
	}
	//Constructor
	public GameLogic()
	{
		playerScore = new int[2]; //black 0 white 		
		stones =  new Stone[7][7];
		tmp_board1 = new Stone[7][7];
		tmp_board2 = new Stone[7][7];
		alreadyChecked = new ArrayList<int[]>();
		toRemove = new ArrayList<int[]>();
		tmpx = 0;
		tmpy = 0;		
		
		this.initStones();
		this.reset();
	}
	
	public void reset()
	{		
		winstatus = WinStatus.NO_WINNER;
		passed = 0;
		tmp_score[0] = 0;
		tmp_score[1] = 0;
		//Set currPlayer Black;
		currPlayer = PieceType.BLACK; //first is black by "convention"
		//Reset stones
		for (int y = 0; y < stones.length; y++)
		{
			for (int x = 0; x < stones[y].length; x++)
			{
				stones[y][x].reset();
				tmp_board1[y][x].reset();
				tmp_board2[y][x].reset();
			}
		}
		//Reset scores
	    this.playerScore[0] = this.playerScore[1] = 0;
	}
	
	public boolean isValidPosition(final int y, final int x)
	{
		return !(y >= 7 || y < 0) && !(x >= 7 || x < 0);
	}
	
	public boolean freeAreTakenByOppositeOrFullFriend(final int y, final int x) //check value of 4 free space arround
	{
		Stone up = null, down = null, right = null, left = null;
		PieceType pt = this.stones[y][x].getPlayer();
		int total = 0, samecolor = 0;
		
		//up
		tmpx = x;
		tmpy = y - 1;
		if (this.isValidPosition(tmpy, tmpx))
			up = this.getStone(tmpy, tmpx);
		//down
		tmpx = x;
		tmpy = y + 1;
		if (this.isValidPosition(tmpy, tmpx))
			down = this.getStone(tmpy, tmpx);
		//left
		tmpx = x - 1;
		tmpy = y;
		if (this.isValidPosition(tmpy, tmpx))
			left = this.getStone(tmpy, tmpx);
		//right		
		tmpx = x + 1;
		tmpy = y;
		if (this.isValidPosition(tmpy, tmpx))
			right = this.getStone(tmpy, tmpx);
		
		//if one is empty return false
		if ((up != null && up.is(PieceType.EMPTY)) ||
				(down != null && down.is(PieceType.EMPTY)) ||
				(left != null && left.is(PieceType.EMPTY)) ||
				(right != null && right.is(PieceType.EMPTY)))
			return false;
		else {
			return true; //else return true
		}
		
	}

	public int checkRemove(final int y, final int x, int value)
	{
		int tmp;
		
		//check if not in the checked list
		for(int i = 0; i < alreadyChecked.size(); i++)
		{
			int[] postmp = alreadyChecked.get(i);
			if (postmp[0] == y && postmp[1] == x)
				return value;				
		}
		if (this.isValidPosition(y, x) && this.getStone(y, x).is(this.getEnnemyColor())) //position valid + ennemy color
		{
			int[] pos = {y,x};
			//Add to the list of checked:
			alreadyChecked.add(pos);
			
			if (this.freeAreTakenByOppositeOrFullFriend(y,x))
				{
					value++;
					toRemove.add(pos);
				}
			else 
				return -1;
			//go to next 4 stones
			//up
			tmpx = x;
			tmpy = y - 1;
			tmp = this.checkRemove(tmpy, tmpx, value);
			if (tmp == -1)
				return -1;
			else
				value = tmp;
			//down
			tmpx = x;
			tmpy = y + 1;
			tmp = this.checkRemove(tmpy, tmpx, value);
			if (tmp == -1)
				return -1;
			else
				value = tmp;
			//left
			tmpx = x - 1;
			tmpy = y;
			tmp = this.checkRemove(tmpy, tmpx, value);
			if (tmp == -1)
				return -1;
			else
				value = tmp;
			//right
			tmpx = x + 1;
			tmpy = y;
			tmp = this.checkRemove(tmpy, tmpx, value);
			if (tmp == -1)
				return -1;
			else
				value = tmp;
			return value;
		}
		else if (this.isValidPosition(y, x) && this.getStone(y, x).is(PieceType.EMPTY))
			return -1;
		else
			return value;
	}
	
	public void deleteLastChain()
	{
		int pos[];
		for (int i = 0; i < toRemove.size(); i++)
		{
			pos = toRemove.get(i);
			this.getStone(pos[0], pos[1]).reset();
		}
	}
	
	public void execCaptureResolver(final int y, final int x)
	{
		int valueUp = 0, valueDown = 0, valueLeft = 0, valueRight = 0;
		
		//up
		alreadyChecked.clear();
		toRemove.clear();
		tmpx = x;
		tmpy = y - 1;
		valueUp = this.checkRemove(tmpy, tmpx, valueUp);
		if (valueUp > 0)
		{
			this.addCurrentPlayerScore(valueUp);
			this.deleteLastChain();
		}
		//down
		alreadyChecked.clear();
		toRemove.clear();
		tmpx = x;
		tmpy = y + 1;
		valueDown= this.checkRemove(tmpy, tmpx, valueDown);
		if (valueDown > 0)
		{
			this.addCurrentPlayerScore(valueDown);
			this.deleteLastChain();
		}
		//left
		alreadyChecked.clear();
		toRemove.clear();
		tmpx = x - 1;
		tmpy = y;
		valueLeft= this.checkRemove(tmpy, tmpx, valueLeft);
		if (valueLeft > 0)
		{
			this.addCurrentPlayerScore(valueLeft);
			this.deleteLastChain();
		}
		//right
		alreadyChecked.clear();
		toRemove.clear();
		tmpx = x + 1;
		tmpy = y;
		valueRight= this.checkRemove(tmpy, tmpx, valueRight);
		
		if (valueRight > 0)
		{
			this.addCurrentPlayerScore(valueRight);
			this.deleteLastChain();
		}
	}
	
	public void placeStone(final int y, final int x)
	{
		//-- Logic here
		if (winstatus == WinStatus.NO_WINNER)
		{			
			//- The game is over, can't play
			if (passed >= 2)
				return ;
	
			//- Can i place here ? Piece already here
			if (this.getStone(y,x).isNot(PieceType.EMPTY))
				return ;
	
			//- Save copy of score if reverse is needed because suicide or ko happen.
			int[] tmp_score = {0,0};
			tmp_score[0] = playerScore[0];
			tmp_score[1] = playerScore[1];
	
			//- Place the piece
			this.getStone(y,x).setPiece(this.currPlayer);
			
			//- Check 4 cases around of this piece and check if all "free" place are take by the current player color
			execCaptureResolver(y, x);
	
			//clear for suicide
			clear_checked();
	
			//- Check for suicide move
			if (!at_least_one_liberty(y, x, currPlayer)) {
				playerScore[0] = tmp_score[0];
				playerScore[1] = tmp_score[1];
				overwrite_board(stones, tmp_board1);
				System.out.println("Forbidden Move : SUICIDE RULE");
			} else if (same_board(tmp_board1, stones) || same_board(tmp_board2, stones)) {
			//- Check for ko move
				playerScore[0] = tmp_score[0];
				playerScore[1] = tmp_score[1];
				overwrite_board(stones, tmp_board1);
				System.out.println("Forbidden Move : KO RULE");
			} else {
			//- Next player
				passed = 0;
				this.nextPlayer();
				overwrite_board(tmp_board2, tmp_board1);
				overwrite_board(tmp_board1, stones);
			}
		}
	}

	private boolean same_board(Stone[][] board1, Stone[][] board2) {
		for (int y = 0; y < 7; y++)
			for (int x = 0; x < 7; x++)
				if (board1[y][x].getPlayer() != board2[y][x].getPlayer())
					return false;
		return true;
	}

	private void clear_checked() {
		for (int i = 0; i < 7; i++)
			for (int j = 0; j < 7; j++)
				stones[i][j].setChecked(false);
	}

	private boolean at_least_one_liberty(int y, int x, PieceType type) {
		stones[y][x].setChecked(true);
		if (stones[y][x].getPlayer() == PieceType.EMPTY)
			return true;
		if (stones[y][x].getPlayer() != type)
			return false;
		if (x < 6 && !stones[y][x + 1].getChecked() && at_least_one_liberty(y, x + 1, type) == true)
			return true;
		if (x > 0 && !stones[y][x - 1].getChecked() && at_least_one_liberty(y, x - 1, type) == true)
			return true;
		if (y < 6 && !stones[y + 1][x].getChecked() && at_least_one_liberty(y + 1, x, type) == true)
			return true;
		if (y > 0 && !stones[y - 1][x].getChecked() && at_least_one_liberty(y - 1, x, type) == true)
			return true;
		return false;
	}

	private void overwrite_board(Stone[][] to_replace, Stone[][] new_board) {
		for (int i = 0; i < to_replace.length; i++)
			for (int j = 0; j < to_replace[i].length; j++)
				to_replace[i][j].setPiece(new_board[i][j].getPlayer());
	}
	
	public Stone[][] getStones()
	{
		return stones;
	}
	
	public void pass() {
		passed++;
		if (passed >= 2) {
			int black_score = playerScore[0];
			int white_score = playerScore[1];
			for (int i = 0; i < stones.length; i++)
				for (int j = 0; j < stones[i].length; j++)
					if (stones[i][j].getPlayer() == PieceType.BLACK)
						black_score++;
					else if (stones[i][j].getPlayer() == PieceType.WHITE)
						white_score++;
			// display win message
			if (black_score > white_score)
				this.winstatus = WinStatus.BLACK_WIN;
			else if (white_score > black_score)
				this.winstatus = WinStatus.WHITE_WIN;
			else 
				this.winstatus = WinStatus.EQUAL_SCORE;
		}
		this.nextPlayer();
	}
	
	public Stone getStone(final int y, final int x)
	{
		return this.stones[y][x];
	}
	
	public void initStones()
	{
		//Init all stones
		for (int i = 0; i < this.stones.length; i++)
		{
			for (int j = 0; j < this.stones[i].length; j++)
			{
				stones[i][j] = new Stone();
				tmp_board1[i][j] = new Stone();
				tmp_board2[i][j] = new Stone();
			}
		}
	}
}
