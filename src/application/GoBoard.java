package application;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Translate;

public class GoBoard extends Pane 
{
	
	
	// rectangle that makes the background of the board
	private Rectangle background;
	// the width and height of a cell in the board
	private double cell_width;
	private double cell_height;
	// arrays for the lines that makeup the horizontal and vertical grid lines
	private Line[] horizontal;
	private Line[] vertical;
	// arrays holding translate objects for the horizontal and vertical grid lines
	private Translate[] horizontal_t;
	private Translate[] vertical_t;
	
	private GameLogic gameLogic;
	private Go cursorForRefesh;
	
	//Getters
	public GameLogic getGameLogic()
	{
		return gameLogic;
	}
	
	//Setter for refresh cursor
	public void setGo(Go instanceGo)
	{
		cursorForRefesh = instanceGo;
	}
	
	//Constructor
	public GoBoard()
	{
		cursorForRefesh = null;
		horizontal = new Line[8];
		vertical = new Line[8];
		horizontal_t = new Translate[8];
		vertical_t = new Translate[8];
		gameLogic = new GameLogic();
		
		//initialising lines, background, render and reset game
		this.initialiseLinesBackground();
		this.initialiseStonesChildrens();
		this.resetGame();
	}

	//Placement of piece
	public void placePiece(double x, double y)
	{
		int posXPiece = 0;
		int posYPiece = 0;
		
		//System.out.println("cell_width" + cell_width + "cell_heigth" + cell_height);
		//System.out.println("Event at: " + x + ", " + y);
		x -= cell_width / 2;
		y -= cell_height / 2;
		
		//Check correct position
		if (x < 0 || y < 0)
			return;
		posXPiece = (int) (x / cell_width);
		posYPiece = (int) (y / cell_height);
		if (posXPiece >= 7 || posYPiece >= 7)
			return ;
			
		//Send position To Game Logic
		gameLogic.placeStone(posYPiece, posXPiece);
		
		//If cursor for refresh is here
		if (cursorForRefesh != null)
		cursorForRefesh.refresh();
	}
	
	//Reset Game
	public void resetGame()
	{
		//Reset GameLogic
		gameLogic.reset();
	}
	
	// private method that will initialise the background and the lines
	private void initialiseLinesBackground() {
		background = new Rectangle();
		background.setFill(Color.SANDYBROWN);
		getChildren().add(background);
		for (int i = 0; i < horizontal.length; i++)
		{
			horizontal[i] = new Line();
			horizontal[i].setStroke(Color.WHITE);
			horizontal[i].setStartX(0);
			horizontal[i].setStartY(0);
			horizontal[i].setEndY(0);
			horizontal_t[i] = new Translate(0, 0);
			horizontal[i].getTransforms().add(horizontal_t[i]);
			getChildren().add(horizontal[i]);
		}
		for (int i = 0; i < vertical.length; i++)
		{
			vertical[i] = new Line();
			vertical[i].setStroke(Color.WHITE);
			vertical[i].setStartX(0);
			vertical[i].setEndX(0);
			vertical[i].setStartY(0);
			vertical_t[i] = new Translate(0, 0);
			vertical[i].getTransforms().add(vertical_t[i]);
			getChildren().add(vertical[i]);
		}
	}
	
	// private method that will add stones to childrens
	private void initialiseStonesChildrens() {
		Stone[][] stones = gameLogic.getStones();
		
		for (int i = 0; i < stones.length; i++)
		{
			for (int j = 0; j < stones[i].length; j++)
			{
				getChildren().add(stones[i][j]);
			}
		}
	}
	
	// overridden version of the resize method to give the board the correct size
	@Override
	public void resize(double width, double height) {
		 super.resize(width, height);
		 this.cell_width = width / (double)8;
		 this.cell_height = height / (double)8;
		 background.setWidth(width);
	     background.setHeight(height);	     
	     horizontalResizeRelocate(width);
	     verticalResizeRelocate(height);
	     pieceResizeRelocate();
	}
	
	// private method for resizing and relocating the horizontal lines
	private void horizontalResizeRelocate(final double width) {
		double newx2 = 0;
		
		for(int i = 0; i < horizontal.length; i++)
		{
			newx2 = (i + 1) * cell_height;
			horizontal[i].setEndX(width);
            horizontal_t[i].setY(newx2);            
        }
	}
		
	// private method for resizing and relocating the vertical lines
	private void verticalResizeRelocate(final double height) {
		double newy2 = 0;
		
		for(int i = 0; i < vertical.length; i++)
		{
			newy2 = (i + 1) * cell_width;
			vertical[i].setEndY(height);
			vertical_t[i].setX(newy2);
        }
	}
	
	// private method for resizing and relocating all the pieces
	private void pieceResizeRelocate() {
		Stone[][] stones = gameLogic.getStones();
		
		for(int i = 0; i < stones.length; i++)
		{
			for(int j = 0; j < stones[i].length; j++)
			{
				stones[i][j].relocate((this.cell_width / 2) + this.cell_width * j , (this.cell_height / 2) + this.cell_height * i);
				stones[i][j].resize(this.cell_width, this.cell_height);
			}
		}
	}
}
