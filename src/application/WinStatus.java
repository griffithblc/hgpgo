package application;

public enum WinStatus
{
	NO_WINNER,
	BLACK_WIN,
	WHITE_WIN,
	EQUAL_SCORE
}
